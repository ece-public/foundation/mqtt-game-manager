const fs = require('fs');
const P = require('bluebird');

/**
 * Write the binary content to a file.
 * @param {String} path 
 * @param {Buffer} content 
 */
async function saveImage(path, content) {
    return await new P((resolve, reject) => {
        fs.writeFile(path, content, (err) => {
            if (err) return reject(err);
            return resolve();
        });
    });
} 

module.exports = saveImage;