const { connect, end, publish } = require('./mqtt');
const FolderWatcher = require('./file-appears');
const path = require('path');
const args = require('./options-parser')['file-transfer']();
const readImage = require('./read-image');
const P = require('bluebird');
const fs = require('fs');
const appears = require('./file-appears');

async function main() {

    const input = path.isAbsolute(args.options.input) ? args.options.input : path.join(process.cwd(), args.options.input);
    const username = args.options.username;
    const password = args.options.password;
    const topic = args.options.topic;

    console.log(`[${new Date().toTimeString()}][INFO] Connecting to the broker with username ${username} ...`);

    // First connect to the broker
    const mqttClient = await connect({
        //url: 'mqtt://localhost:2019',
        url: 'mqtt://foundation.lyon.ece.licot.fr:2019',
        username,
        password,
    });

    console.log(`[${new Date().toTimeString()}][INFO] Watching files created in ${input} ...`);

    const watcher = fs.watch(input, { encoding: 'utf-8' });

    watcher.on('change', async (event, filename) => {
        if (event === 'rename') {
            filename = path.join(input, filename);
            const status = await appears(filename);
            switch (status) {
                case 'appears':
                    console.log(`[${new Date().toTimeString()}][INFO] File ${filename} created in watched folder`);
                    const content = await readImage(filename);
                    await publish(mqttClient, topic, content);
                    console.log(`[${new Date().toTimeString()}][INFO] File ${filename} transmitted`);
                    break;
                case 'disappears':
                    console.log(`[${new Date().toTimeString()}][INFO] File ${filename} was deleted`);
                    break;
            }
        }
    });

    // catch Ctrl-C to perform cleaning up
    process.on('SIGINT', async (p, t) => {
        console.log(`[${new Date().toTimeString()}][INFO] Cleaning up ...`);
        if (mqttClient) {
            await end(mqttClient);
        }
        if (watcher) {
            watcher.close();
        }
    });

}

main().catch(err => console.error(err));
