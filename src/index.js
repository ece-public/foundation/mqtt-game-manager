const { connect, end, publish } = require('./mqtt');
const path = require('path');
const args = require('./options-parser')['game-manager']();
const readImage = require('./read-image');
const saveImage = require('./save-image');
const P = require('bluebird');

async function main() {

    // First connect to the broker
    const mqttClient = await connect({
        //url: 'mqtt://localhost:2019',
        url: 'mqtt://foundation.lyon.ece.licot.fr:2019',
        username: 'admin',
        password: 'xrHOUt831837D5jEfqnL',
    });

    console.log(`[${new Date().toTimeString()}][INFO] Subscribing to the topics ...`);
    mqttClient.subscribe('foundation/start-of-the-game');
    mqttClient.subscribe('foundation/stop-of-the-game');

    console.log(`[${new Date().toTimeString()}][INFO] Registering listener ...`);
    mqttClient.on('message', async function (topic, message) {

        console.log(`[${new Date().toTimeString()}][RECEIVER] Message received on topic :` + topic)

        if (topic === 'foundation/start-of-the-game') {
            // if starting the game, the received payload is a binary image.
            const outputFilePath = path.join(args.options.output, `${Date.now()}.png`);
            saveImage(outputFilePath, message)
                .then(() => console.log(`[${new Date().toTimeString()}][RECEIVER] Image written at path ${outputFilePath}`));

        } else if (topic === 'foundation/stop-of-the-game') {

            try {
                // We first try to convert the message into an object.
                const payload = JSON.parse(message.toString());
                console.log(`[${new Date().toTimeString()}][RECEIVER] The message payload is a JSON containing :`, payload);
            } catch (e) {
                const payload = message.toString();
                console.log(`[${new Date().toTimeString()}][RECEIVER] The message payload is not a JSON, it can be a raw buffer or a string, containing:`, payload);
            }

            end(mqttClient).finally(() => process.exit(0));
        } else {
            console.log(`[${new Date().toTimeString()}][RECEIVER] Unknown message received :`, message);
        }

    })

    // Defines helpers methods :
    function startTheGame(payload) {
        console.log(`[${new Date().toTimeString()}][INFO] Game is started`);
        return publish(mqttClient, 'foundation/start-of-the-game', payload);
    }

    function stopTheGame(payload = {}) {
        console.log(`[${new Date().toTimeString()}][INFO] The game is ended.`);
        payload = JSON.stringify(Object.assign(payload, { at: Date.now() }));
        return publish(mqttClient, 'foundation/stop-of-the-game', payload);
    }


    try {
        // Get the image content binary
        const binary = await readImage(args.options.map);

        // Start the game with the image content
        await startTheGame(binary);
        console.log(`[${new Date().toTimeString()}][INFO] The game will end in ${args.options.duration}s starting from now.`);

    } catch (e) {
        await end(mqttClient);
        throw e;
    }


    // Configure a timeout that will stop the game after the configured duration.
    let timeout = setTimeout(() => {
        stopTheGame().catch((e) => {
            console.error(e);
            end(mqttClient).finally(() => process.exit(-1))
        });
        timeout = null;
    }, args.options.duration * 1000);

    // catch Ctrl-C to perform cleaning up
    process.on('SIGINT', async (p, t) => {

        if (timeout && timeout._onTimeout) {
            console.log(`[${new Date().toTimeString()}][INFO] Exiting before the end of the game.`);
            console.log(`[${new Date().toTimeString()}][INFO] Cleaning up ...`);
            clearTimeout(timeout);
            try {
                await stopTheGame();
            } catch (e) {
                await end(mqttClient); // Disconnect from broker if stopping the game has failed
            }
        }

    });

}

main().catch(err => console.error(err));
