const fs = require('fs');
const P = require('bluebird');

async function appears(filename) {
    return new P((resolve, reject) => {
        fs.access(filename, fs.constants.F_OK, (err) => {
            if (err) resolve('disappears');
            else resolve('appears');
        });
    });
}

module.exports = appears;