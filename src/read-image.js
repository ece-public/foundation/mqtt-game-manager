const fs = require('fs');
const P = require('bluebird');

/**
 * Get the binary content of a file.
 * @param {String} path 
 */
async function readImage(path) {
    return await new P((resolve, reject) => {
        fs.readFile(path, (err, content) => {
            if (err) return reject(err);
            return resolve(content);
        });
    });
} 

module.exports = readImage;