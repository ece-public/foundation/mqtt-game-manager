const mqtt = require('mqtt');
const P = require('bluebird');

async function connect({ url, username, password, }) {
    console.log(`[INFO] Connecting to the broker ...`);

    const client = mqtt.connect(url, {
        rejectUnauthorized: false,
        username,
        password,
    });

    await new P((resolve, reject) => {

        const rejecter = (err) => {
            client.removeListener('connect', resolver);
            reject(err);
        }

        const resolver = () => {
            client.removeListener('error', rejecter);
            resolve();
        }

        client.once('connect', resolver);
        client.once('error', rejecter);
    });

    return client;
}

async function end(client) {
    console.log(`[INFO] Disconnecting from broker ...`);
    return new P((resolve, reject) => {
        client.end((err) => {
            if (err) return reject(err);
            resolve();
        });
    });
}

async function publish(client, topic, payload) {
    return new P((resolve, reject) => {
        client.publish(topic, payload, (err) => {
            if (err) return reject(err);
            resolve();
        });
    });
}

module.exports = {
    connect,
    end,
    publish,
}