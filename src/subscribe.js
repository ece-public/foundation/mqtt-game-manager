
const { connect, end, publish } = require('./mqtt');
const path = require('path');
const args = require('./options-parser')['file-receive']();
const fs = require('fs');
const saveImage = require('./save-image');

async function main() {

    const output = path.isAbsolute(args.options.output) ? args.options.output : path.join(process.cwd(), args.options.output);
    const username = args.options.username;
    const password = args.options.password;
    const topic = args.options.topic;

    console.log(`[${new Date().toTimeString()}][INFO] Connecting to the broker with username ${username} ...`);

    // First connect to the broker
    const mqttClient = await connect({
        //url: 'mqtt://localhost:2019',
        url: 'mqtt://foundation.lyon.ece.licot.fr:2019',
        username,
        password,
    });

    console.log(`[${new Date().toTimeString()}][INFO] Subscribing to the topic ${topic} ...`);
    mqttClient.subscribe(topic);

    console.log(`[${new Date().toTimeString()}][INFO] Registering listener ...`);
    mqttClient.on('message', async function (topic, message) {
        console.log(`[${new Date().toTimeString()}][RECEIVER] File received`);
        saveImage(output, message)
            .then(() => console.log(`[${new Date().toTimeString()}][RECEIVER] File written at path ${output}`));

    });

    // catch Ctrl-C to perform cleaning up
    process.on('SIGINT', async (p, t) => {
        console.log(`[${new Date().toTimeString()}][INFO] Cleaning up ...`);
        if (mqttClient) {
            await end(mqttClient);
        }
        try { fs.unlinkSync(output); } catch (e) { }
    });

}

main().catch(err => console.error(err));
