const argv = require('argv');

// export the options
module.exports = {
    'game-manager': () => {
        argv.version('v1.1');
        argv.info('Tool to start a game with a map and to stop it with ctrl+C');
        return argv.option([
            {
                name: 'map',
                short: 'm',
                description: 'Set the path to the image send at the start of the game',
                type: 'path'
            },
            {
                name: 'output',
                short: 'o',
                description: 'Set the file path where the received image will be stored',
                type: 'path'
            },
            {
                name: 'duration',
                short: 'd',
                description: 'Set the duration of the game (in seconds)',
                type: 'number'
            },
        ]).run();
    },
    'file-transfer': () => {
        argv.version('v1.0');
        argv.info('Tool to send an file from a watched folder to a topic on the broker. Exit with ctrl+C');
        return argv.option([
            {
                name: 'input',
                short: 'i',
                description: 'Set the path to the watched folder',
                type: 'path'
            },
            {
                name: 'topic',
                short: 't',
                description: 'Set the topic on which to send the file content',
                type: 'string'
            },
            {
                name: 'username',
                short: 'u',
                description: 'Set the username to authenticate on the broker',
                type: 'string'
            },
            {
                name: 'password',
                short: 'p',
                description: 'Set the password to authenticate on the broker',
                type: 'string'
            },
        ]).run();
    },
    'file-receive': () => {
        argv.version('v1.0');
        argv.info('Tool to receive a file on a topic, and save at the specified output location. Exit with ctrl+C');
        return argv.option([
            {
                name: 'output',
                short: 'o',
                description: 'Set the path to the created file (complete path with filename)',
                type: 'path'
            },
            {
                name: 'topic',
                short: 't',
                description: 'Set the topic to subscribe on.',
                type: 'string'
            },
            {
                name: 'username',
                short: 'u',
                description: 'Set the username to authenticate on the broker',
                type: 'string'
            },
            {
                name: 'password',
                short: 'p',
                description: 'Set the password to authenticate on the broker',
                type: 'string'
            },
        ]).run();
    }
}
