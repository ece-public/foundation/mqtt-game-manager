
## Pre-requisites

* node v10.9.0
* npm v6.13.4

You must know how to open a command-line terminal on your computer.

First install the sources and the dependencies :

```
git clone https://gitlab.com/ece-public/foundation/mqtt-game-manager.git
cd mqtt-game-manager
npm install
```

## How to use it 

### File Watcher

The file watcher will send every created file in a folder to the specified topic over mqtt.  
The file will be send in the same manner as the one on `foundation/start-of-the-game`.

Start watching a folder by running the below command :   

```
npm run watch -- --input=<path/to/input/folder> --username=<squad-username> --password=<squad-password> --topic='<community/topic>'    
```

> Replace the username, password and topic with your squads credentials and the desired community topic.
> The input folder must exists.

You can start as many watchers as you like.

### Topic Subscriber

The topic subscriber will save every message received on the specified topic into the output file given as argument 
to the command line.  
On every message the same file will be completly rewrite.  
On exit, the file will be deleted.  

Start subscribing to a topic by running the below command :   

```
npm run subscribe -- --output=<path/to/output/file.dat> --username=<squad-username> --password=<squad-password> --topic='<cX-community/topic>'    
```

> Replace the username, password and topic with your squads credentials and the desired community topic.
> The path to the file must exists. The file itself will be created on first reception.

You can start as many subscribers as you like.


### Game Manager

Start the game with a image, by choosing an image from the `maps/` folder, choosing an output folder where the received image at the start of the game will be stored, and the duration of the game is seconds.

```
npm start -- --map=maps/I2FDT-PLG-10.png --output=output/ --duration=120
```

After the duration elapsed the tool will end the game. Or if you want to quit before just press `Ctrl+C` to exit the script. 